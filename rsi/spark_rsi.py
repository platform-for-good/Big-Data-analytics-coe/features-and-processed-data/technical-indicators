#spark-submit spark_rsi.py -p 14 -ob 70 -os 30 -s ../../../storage/s1/securities/1d/ -d ../../../storage/s2/rsi/
from datetime import datetime
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, DateType, DoubleType
from argparse import ArgumentParser
import pandas as pd, os

parser = ArgumentParser(description='RSI')
parser.add_argument('-t', '--tickers', default='DJIX', type=str, required=False, help='Used to look up a specific tickers. Commma seperated. Example: MSFT,AAPL,AMZN')
parser.add_argument('-p', '--period', default=14, type=int, required=False, help='Look back period')
parser.add_argument('-ob', '--overbought', default=70, type=int, required=False, help='Overbought level')
parser.add_argument('-os', '--oversold', default=30, type=int, required=False, help='Oversold level')
parser.add_argument('-s', '--src', default='../../../storage/s1/securities/1d/', type=str, required=False, help='Source directory of the data to be found.')
parser.add_argument('-d', '--dest', default='../../../storage/s2/rsi/', type=str, required=False, help='Destination directory of the processed to be stored.')

args = parser.parse_args()
print("Tickers " + args.tickers)
print("period " + str(args.period))
print("Oversold " + str(args.oversold))
print("Overbought " + str(args.overbought))

print("Source " + args.src)
print("Destination " + args.dest)

period = args.period
oversold = args.oversold
overbought = args.overbought

if (args.tickers == "DJIX"):
	tickers = ['MMM','AXP','AMGN','AAPL','BA','CAT','CVX','CSCO','KO','DOW','GS','HD','HON','IBM','INTC','JNJ','JPM','MCD','MRK','MSFT','NKE','PG','CRM','TRV','UNH','VZ','V','WBA','WMT','DIS']
elif (args.tickers == "SPX"):
    tickers = ["MMM","ABT","ABBV","ABMD","ACN","ATVI","ADBE","AMD","AAP","AES","AFL","A","APD","AKAM","ALK","ALB","ARE","ALXN","ALGN","ALLE","LNT","ALL","GOOGL","GOOG","MO","AMZN","AMCR","AEE","AAL","AEP","AXP","AIG","AMT","AWK","AMP","ABC","AME","AMGN","APH","ADI","ANSS","ANTM","AON","AOS","APA","AAPL","AMAT","APTV","ADM","ANET","AJG","AIZ","T","ATO","ADSK","ADP","AZO","AVB","AVY","BKR","BLL","BAC","BK","BAX","BDX","BRK.B","BBY","BIO","BIIB","BLK","BA","BKNG","BWA","BXP","BSX","BMY","AVGO","BR","BF.B","CHRW","COG","CDNS","CPB","COF","CAH","KMX","CCL","CARR","CTLT","CAT","CBOE","CBRE","CDW","CE","CNC","CNP","CERN","CF","SCHW","CHTR","CVX","CMG","CB","CHD","CI","CINF","CTAS","CSCO","C","CFG","CTXS","CLX","CME","CMS","KO","CTSH","CL","CMCSA","CMA","CAG","COP","ED","STZ","COO","CPRT","GLW","CTVA","COST","CCI","CSX","CMI","CVS","DHI","DHR","DRI","DVA","DE","DAL","XRAY","DVN","DXCM","FANG","DLR","DFS","DISCA","DISCK","DISH","DG","DLTR","D","DPZ","DOV","DOW","DTE","DUK","DRE","DD","DXC","EMN","ETN","EBAY","ECL","EIX","EW","EA","EMR","ENPH","ETR","EOG","EFX","EQIX","EQR","ESS","EL","ETSY","EVRG","ES","RE","EXC","EXPE","EXPD","EXR","XOM","FFIV","FB","FAST","FRT","FDX","FIS","FITB","FE","FRC","FISV","FLT","FLIR","FLS","FMC","F","FTNT","FTV","FBHS","FOXA","FOX","BEN","FCX","GPS","GRMN","IT","GD","GE","GIS","GM","GPC","GILD","GL","GPN","GS","GWW","HAL","HBI","HIG","HAS","HCA","PEAK","HSIC","HSY","HES","HPE","HLT","HFC","HOLX","HD","HON","HRL","HST","HWM","HPQ","HUM","HBAN","HII","IEX","IDXX","INFO","ITW","ILMN","INCY","IR","INTC","ICE","IBM","IP","IPG","IFF","INTU","ISRG","IVZ","IPGP","IQV","IRM","JKHY","J","JBHT","SJM","JNJ","JCI","JPM","JNPR","KSU","K","KEY","KEYS","KMB","KIM","KMI","KLAC","KHC","KR","LB","LHX","LH","LRCX","LW","LVS","LEG","LDOS","LEN","LLY","LNC","LIN","LYV","LKQ","LMT","L","LOW","LUMN","LYB","MTB","MRO","MPC","MKTX","MAR","MMC","MLM","MAS","MA","MKC","MXIM","MCD","MCK","MDT","MRK","MET","MTD","MGM","MCHP","MU","MSFT","MAA","MHK","TAP","MDLZ","MPWR","MNST","MCO","MS","MOS","MSI","MSCI","NDAQ","NTAP","NFLX","NWL","NEM","NWSA","NWS","NEE","NLSN","NKE","NI","NSC","NTRS","NOC","NLOK","NCLH","NOV","NRG","NUE","NVDA","NVR","ORLY","OXY","ODFL","OMC","OKE","ORCL","OTIS","PCAR","PKG","PH","PAYX","PAYC","PYPL","PNR","PBCT","PEP","PKI","PRGO","PFE","PM","PSX","PNW","PXD","PNC","POOL","PPG","PPL","PFG","PG","PGR","PLD","PRU","PEG","PSA","PHM","PVH","QRVO","PWR","QCOM","DGX","RL","RJF","RTX","O","REG","REGN","RF","RSG","RMD","RHI","ROK","ROL","ROP","ROST","RCL","SPGI","CRM","SBAC","SLB","STX","SEE","SRE","NOW","SHW","SPG","SWKS","SLG","SNA","SO","LUV","SWK","SBUX","STT","STE","SYK","SIVB","SYF","SNPS","SYY","TMUS","TROW","TTWO","TPR","TGT","TEL","TDY","TFX","TER","TSLA","TXN","TXT","TMO","TJX","TSCO","TT","TDG","TRV","TRMB","TFC","TWTR","TYL","TSN","UDR","ULTA","USB","UAA","UA","UNP","UAL","UNH","UPS","URI","UHS","UNM","VLO","VAR","VTR","VRSN","VRSK","VZ","VRTX","VFC","VIAC","VTRS","V","VNT","VNO","VMC","WRB","WAB","WMT","WBA","DIS","WM","WAT","WEC","WFC","WELL","WST","WDC","WU","WRK","WY","WHR","WMB","WLTW","WYNN","XEL","XRX","XLNX","XYL","YUM","ZBRA","ZBH","ZION","ZTS"]
else:
	tickers = args.tickers.split(",")

spark = SparkSession.builder.appName("rsi").getOrCreate()

DATE = 'Date'
OPEN = 'Open'
HIGH = 'High'
LOW = 'Low'
CLOSE = 'Close'
ADJUSTED_CLOSE = 'Adj Close'
VOLUME = 'Volume'

schema = (StructType([
    StructField(DATE,DateType(),True),               # 0
    StructField(OPEN,DoubleType(),True),             # 1
    StructField(HIGH,DoubleType(),True),             # 2
    StructField(LOW,DoubleType(),True),              # 3
    StructField(CLOSE,DoubleType(),True),            # 4
    StructField(ADJUSTED_CLOSE,DoubleType(),True),   # 5
    StructField(VOLUME,IntegerType(),True)           # 6
    ]))

list_files = [];
for ticker in tickers:
    list_files.append(args.src + ticker + '.csv')
# print(list_files)

# Get the path for each stock file in a list
# list_files = (glob.glob(args.src + '*.csv'))
# print(list_files)

# You can use this line to limit the analysis to a portion of the stocks in the "stocks folder"
# list_files = list_files[:1]

# Create the dataframe that we will be adding the final analysis of each stock to
Compare_Stocks = pd.DataFrame(columns=["Company", "Days_Observed", "Crosses", "True_Positive", "False_Positive", "True_Negative", "False_Negative", "Sensitivity",
"Specificity", "Accuracy", "TPR", "FPR"])

# While loop to cycle through the stock paths
for stock in list_files:
    print(stock);

    df = spark.read.format('csv') \
        .schema(schema) \
        .option("header", "true") \
        .option("sep", ",") \
        .load(stock)

    Hist_data = df.toPandas()

    # Dataframe to hold the historical data of the stock we are interested in.
    # Hist_data = pd.read_csv(stock)
    company = ((os.path.basename(stock)).split(".csv")[0])  # Name of the company

    # This list holds the closing prices of a stock
    prices = []
    c = 0

    # Add the closing prices to the prices list and make sure we start at greater than 2 dollars to reduce outlier calculations.
    while c < len(Hist_data):
        if Hist_data.iloc[c,4] > float(2.00):  # Check that the closing price for this day is greater than $2.00
            prices.append(Hist_data.iloc[c,4])
        c += 1

    # prices_df = pd.DataFrame(prices)  # Make a dataframe from the prices list
    i = 0
    upPrices=[]
    downPrices=[]

    #  Loop to hold up and down price movements
    while i < len(prices):
        if i == 0:
            upPrices.append(0)
            downPrices.append(0)
        else:
            if (prices[i]-prices[i-1])>0:
                upPrices.append(prices[i]-prices[i-1])
                downPrices.append(0)
            else:
                downPrices.append(prices[i]-prices[i-1])
                upPrices.append(0)
        i += 1
    x = 0
    avg_gain = []
    avg_loss = []

    #  Loop to calculate the average gain and loss
    limit = period + 1
    while x < len(upPrices):
        if x < limit:
            avg_gain.append(0)
            avg_loss.append(0)
        else:
            sumGain = 0
            sumLoss = 0
            y = x - period
            while y<=x:
                sumGain += upPrices[y]
                sumLoss += downPrices[y]
                y += 1
            avg_gain.append(sumGain/period)
            avg_loss.append(abs(sumLoss/period))
        x += 1
    p = 0
    RS = []
    RSI = []

    #  Loop to calculate RSI and RS
    while p < len(prices):
        if p < limit:
            RS.append(0)
            RSI.append(0)
        else:
            RSvalue = (avg_gain[p]/avg_loss[p])
            RS.append(RSvalue)
            RSI.append(100 - (100/(1+RSvalue)))
        p+=1

    #  Creates the csv for each stock's RSI and price movements
    df_dict = {
        'Prices' : prices,
        'upPrices' : upPrices,
        'downPrices' : downPrices,
        'AvgGain' : avg_gain,
        'AvgLoss' : avg_loss,
        'RS' : RS,
        'RSI' : RSI
    }

    pdf = pd.DataFrame(df_dict, columns = ['Prices', 'upPrices', 'downPrices', 'AvgGain','AvgLoss', 'RS', "RSI"])
    # pdf.to_csv(args.dest + Company+"_RSI.csv", index = False)

    df = spark.createDataFrame(pdf)
    df.write.format("csv").option("header", "true").option("delimiter", ",").mode('Overwrite').save(args.dest + company)

    #  Code to test the accuracy of the RSI at predicting stock prices
    Days_Observed = limit
    Crosses = 0
    nothing = 0
    True_Positive = 0
    False_Positive = 0
    True_Negative = 0
    False_Negative = 0
    Sensitivity = 0
    Specificity = 0
    Accuracy = 0

    while Days_Observed < len(prices) - 5:
        if RSI[Days_Observed] <= oversold:
            if ((prices[Days_Observed + 1] + prices[Days_Observed + 2] + prices[Days_Observed + 3] + prices[
                Days_Observed + 4] + prices[Days_Observed + 5]) / 5) > prices[Days_Observed]:
                True_Positive += 1
            else:
                False_Negative += 1
            Crosses += 1
        elif RSI[Days_Observed] >= overbought:
            if ((prices[Days_Observed + 1] + prices[Days_Observed + 2] + prices[Days_Observed + 3] + prices[
                Days_Observed + 4] + prices[Days_Observed + 5]) / 5) <= prices[Days_Observed]:
                True_Negative += 1
            else:
                False_Positive += 1
            Crosses += 1
        else:
            # Do nothing
            nothing += 1
        Days_Observed += 1
    # while Days_Observed<len(prices)-5:

    #     Days_Observed += 1
    try:
        Sensitivity = (True_Positive / (True_Positive + False_Negative))  # Calculate sensitivity
    except ZeroDivisionError:  # Catch the divide by zero error
        Sensitivity = 0
    try:
        Specificity = (True_Negative / (True_Negative + False_Positive))  # Calculate specificity
    except ZeroDivisionError:
        Specificity = 0
    try:
        Accuracy = (True_Positive + True_Negative) / (
                    True_Negative + True_Positive + False_Positive + False_Negative)  # Calculate accuracy
    except ZeroDivisionError:
        Accuracy = 0
    TPR = Sensitivity  # Calculate the true positive rate
    FPR = 1 - Specificity  # Calculate the false positive rate
    # Create a row to add to the compare_stocks
    add_row = {'Company': company, 'Days_Observed': Days_Observed, 'Crosses': Crosses, 'True_Positive': True_Positive,
               'False_Positive': False_Positive,
               'True_Negative': True_Negative, 'False_Negative': False_Negative, 'Sensitivity': Sensitivity,
               'Specificity': Specificity, 'Accuracy': Accuracy, 'TPR': TPR, 'FPR': FPR}

    Compare_Stocks = Compare_Stocks.append(add_row, ignore_index=True)  # Add the analysis on the stock to the existing Compare_Stocks dataframe
    Compare_Stocks.to_csv(args.dest + datetime.today().strftime('%Y-%m-%d') + '_summary.csv', index=False)  # Save the compiled data on each stock to a csv

    # df2 = spark.createDataFrame(Compare_Stocks)
    # df2.write.format("csv").option("header", "true").option("delimiter", ",").mode('Overwrite').save(args.dest + company + "_summary")

