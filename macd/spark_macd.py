#spark-submit spark_macd.py -p 14 -ob 70 -os 30 -s ../../../storage/s1/securities/1d/ -d ../../../storage/s2/rsi/
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, DateType, DoubleType
from argparse import ArgumentParser
import pandas as pd, glob, os

parser = ArgumentParser(description='MACD')
parser.add_argument('-t', '--tickers', default='DJIX', type=str, required=False, help='Used to look up a specific tickers. Commma seperated. Example: MSFT,AAPL,AMZN')
parser.add_argument('-p', '--period', default=14, type=int, required=False, help='Look back period')
parser.add_argument('-ob', '--overbought', default=70, type=int, required=False, help='Overbought level')
parser.add_argument('-os', '--oversold', default=30, type=int, required=False, help='Oversold level')
parser.add_argument('-s', '--src', default='../../../storage/s1/securities/1d/', type=str, required=False, help='Source directory of the data to be found.')
parser.add_argument('-d', '--dest', default='../../../storage/s2/macd/', type=str, required=False, help='Destination directory of the processed to be stored.')

args = parser.parse_args()
print("Tickers " + args.tickers)
print("period " + str(args.period))
print("Oversold " + str(args.oversold))
print("Overbought " + str(args.overbought))

print("Source " + args.src)
print("Destination " + args.dest)

period = args.period
oversold = args.oversold
overbought = args.overbought

if (args.tickers=="DJIX"):
	tickers = ['MMM','AXP','AMGN','AAPL','BA','CAT','CVX','CSCO','KO','DOW','GS','HD','HON','IBM','INTC','JNJ','JPM','MCD','MRK','MSFT','NKE','PG','CRM','TRV','UNH','VZ','V','WBA','WMT','DIS']
else:
	tickers = args.tickers.split(",")

spark = SparkSession.builder.appName("macd").getOrCreate()

DATE = 'Date'
OPEN = 'Open'
HIGH = 'High'
LOW = 'Low'
CLOSE = 'Close'
ADJUSTED_CLOSE = 'Adj Close'
VOLUME = 'Volume'

schema = (StructType([
    StructField(DATE, DateType(), True),               # 0
    StructField(OPEN, DoubleType(), True),             # 1
    StructField(HIGH, DoubleType(), True),             # 2
    StructField(LOW, DoubleType(), True),              # 3
    StructField(CLOSE, DoubleType(), True),            # 4
    StructField(ADJUSTED_CLOSE, DoubleType(), True),   # 5
    StructField(VOLUME, IntegerType(), True)           # 6
    ]))

list_files = [];
for ticker in tickers:
    list_files.append(args.src + ticker + '.csv')
print(list_files)

# Get the path for each stock file in a list
# list_files = (glob.glob(args.src + '*.csv'))
# print(list_files)

# You can use this line to limit the analysis to a portion of the stocks in the "stocks folder"
# list_files = list_files[:1]

# Create the dataframe that we will be adding the final analysis of each stock to
Compare_Stocks = pd.DataFrame(columns=["Company", "Days_Observed", "Crosses", "True_Positive", "False_Positive", "True_Negative", "False_Negative", "Sensitivity",
"Specificity", "Accuracy", "TPR", "FPR"])

# While loop to cycle through the stock paths
for stock in list_files:
    print(stock);

    df = spark.read.format('csv') \
        .schema(schema) \
        .option("header", "true") \
        .option("sep", ",") \
        .load(stock)

    Hist_data = df.toPandas()
    print(Hist_data);

    # Dataframe to hold the historical data of the stock we are interested in.
    # Hist_data = pd.read_csv(stock)
    Company = ((os.path.basename(stock)).split(".csv")[0])  # Name of the company

    # This list holds the closing prices of a stock
    prices = []
    c = 0

    # Add the closing prices to the prices list and make sure we start at greater than 2 dollars to reduce outlier calculations.
    while c < len(Hist_data):
        if Hist_data.iloc[c, 4] > float(2.00):  # Check that the closing price for this day is greater than $2.00
            prices.append(Hist_data.iloc[c, 4])
        c += 1

    print("prices")
    prices_df = pd.DataFrame(prices)  # Make a dataframe from the prices list
    i = 0
    upPrices = []
    downPrices = []

    # Calculate exponentiall weighted moving averages:
    day12 = prices_df.ewm(span=12).mean()  #
    day26 = prices_df.ewm(span=26).mean()
    print(prices)

    macd = []  # List to hold the MACD line values
    counter = 0  # Loop to substantiate the MACD line
    while counter < (len(day12)):
        macd.append(day12.iloc[counter, 0] - day26.iloc[counter, 0])  # Subtract the 26 day EW moving average from the 12 day.
        counter += 1
    macd_df = pd.DataFrame(macd)
    signal_df = macd_df.ewm(span=9).mean() # Create the signal line, which is a 9 day EW moving average
    signal = signal_df.values.tolist()  # Add the signal line values to a list.
    #  Loop to Compare the expected MACD crosses results to the actual results
    Day = 1

    ########################################################
    # Determining the accuracy of each stock
    ########################################################
    #  Code to test the accuracy of MACD at predicting stock prices
    Days_Observed = 0
    Crosses = 0
    nothing = 0
    True_Positive = 0
    False_Positive = 0
    True_Negative = 0
    False_Negative = 0
    Sensitivity = 0
    Specificity = 0
    Accuracy = 0

    count = 0

    while Day < len(
            macd) - 5:  # -1 to be able to use the last day for prediction, -5 so we can look at the 5 day post average.
        Prev_Day = Day - 1
        # Avg_Closing_Next_Days = (prices[Day+1] + prices[Day+2] + prices[Day+3] + prices[Day+4] + prices[Day+5])/5 # To use 5 day average as a decision.
        Avg_Closing_Next_Days = (prices[Day + 1] + prices[Day + 2] + prices[
            Day + 3]) / 3  # To use 3 day average as a decision.
        Days_Observed += 1  # Count how many days were observed
        if ((signal[Prev_Day] > macd[Prev_Day]) and (signal[Day] <= macd[
            Day])):  # when the signal line dips below the macd line (Expected increase over the next x days)
            Crosses += 1  # register that a cross occurred
            if (prices[Day] < Avg_Closing_Next_Days):  # Tests if the price increases over the next x days.
                True_Positive += 1
            else:
                False_Negative += 1

        if ((signal[Prev_Day] < macd[Prev_Day]) and (signal[Day] >= macd[
            Day])):  # when the signal line moves above the macd line (Expected dip over the next x days)
            Crosses += 1
            if (prices[Day] > Avg_Closing_Next_Days):  # Tests if the price decreases over the next x days.
                True_Negative += 1
            else:
                False_Positive += 1
        Day += 1
    try:
        Sensitivity = (True_Positive / (True_Positive + False_Negative))  # Calculate sensitivity
    except ZeroDivisionError:  # Catch the divide by zero error
        Sensitivity = 0
    try:
        Specificity = (True_Negative / (True_Negative + False_Positive))  # Calculate specificity
    except ZeroDivisionError:
        Specificity
    try:
        Accuracy = (True_Positive + True_Negative) / (
                    True_Negative + True_Positive + False_Positive + False_Negative)  # Calculate accuracy
    except ZeroDivisionError:
        Accuracy = 0
    TPR = Sensitivity  # Calculate the true positive rate
    FPR = 1 - Specificity  # Calculate the false positive rate
    # Create a row to add to the compare_stocks
    add_row = {'Company': Company, 'Days_Observed': Days_Observed, 'Crosses': Crosses, 'True_Positive': True_Positive,
               'False_Positive': False_Positive,
               'True_Negative': True_Negative, 'False_Negative': False_Negative, 'Sensitivity': Sensitivity,
               'Specificity': Specificity, 'Accuracy': Accuracy, 'TPR': TPR, 'FPR': FPR}
    Compare_Stocks = Compare_Stocks.append(add_row,
                                           ignore_index=True)  # Add the analysis on the stock to the existing Compare_Stocks dataframe
    count += 1
    Compare_Stocks.to_csv("All_Stocks.csv", index=False)  # Save the compiled data on each stock to a csv - All_Stocks.csv


    ########################################################
    # Ranking the Stocks
    ########################################################

    Compare_Stocks = pd.read_csv("All_Stocks.csv") # Read in the All_Stocks data to a dataframe
    # Delete companies that don't have enough crosses observed. I am using 50 crosses as my cuttoff:
    Not_Enough_Records = []
    Row = 0
    while Row < (len(Compare_Stocks)):
        if Compare_Stocks.iloc[Row, 2] < 50:
            Not_Enough_Records.append(Row)
        Row += 1
    Compare_Stocks = Compare_Stocks.drop(Not_Enough_Records)  # Remove records that do not have enough crosses for us to observe
    Avg_Accuracy = []  # List to hold the accuracy of each stock
    i=0
    while i < (len(Compare_Stocks)):
        Avg_Accuracy.append(Compare_Stocks.iloc[i,9])
        i += 1

    # Create a dataframe from Compare_Stocks
    df = Compare_Stocks[['Company', 'Days_Observed', 'Crosses', 'True_Positive', 'False_Positive', 'True_Negative', 'False_Negative', 'Sensitivity', 'Specificity', 'TPR', 'FPR', 'Accuracy']]
    df["Companies_Ranked"] = df["Accuracy"].rank(ascending=False)  # Rank the stocks by their Accuracy
    df.sort_values("Accuracy", inplace=True, ascending=False)  # Sort the ranked stocks
    df.to_csv("Summary_5_Day_Avg_26_12_MACD.csv", index=False)  # Save the dataframe to a csv
    # We now have a list of stocks ranked by how well the MACD indicator predicts their price change.
    # print("The average accuracy of all stocks observed: " + str(mean(Avg_Accuracy)))  # The overall accuracy of the MACD.