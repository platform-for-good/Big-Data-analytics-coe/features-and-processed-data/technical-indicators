import pandas as pd
from textblob import TextBlob
import xml.etree.ElementTree as et
import seaborn as sns
sns.set(rc={'figure.figsize':(11.7,8.27)})

# from google.colab import drive
# drive.mount('/content/drive')

from os import listdir
from os.path import isfile, join
path = 'news/'
onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
len(onlyfiles)

i = 0
for item in onlyfiles:
  if i < 10:
    print(item)
    i = i + 1

ticker = []
year = []
month = []
date = []
timestamp = []
guid = []
description = []
link = []
pubDate = []
title = []

error_counter = 0

for filename in onlyfiles:
    print("Processing file: " + filename)

    try:
        _ticker = ""
        _year = ""
        _month = ""
        _date = ""
        _timestamp = ""

        if filename[1].isnumeric():
            _ticker = filename[:1]
            _year = filename[1:5]
            _month = filename[5:8]
            _date = filename[8:10]
            _timestamp = filename[10:14]
        elif filename[2].isnumeric():
            _ticker = filename[:2]
            _year = filename[2:6]
            _month = filename[6:9]
            _date = filename[9:11]
            _timestamp = filename[11:15]
        elif filename[3].isnumeric():
            _ticker = filename[:3]
            _year = filename[3:7]
            _month = filename[7:10]
            _date = filename[10:12]
            _timestamp = filename[12:16]
        else:
            _ticker = filename[:4]
            _year = filename[4:8]
            _month = filename[8:11]
            _date = filename[11:13]
            _timestamp = filename[13:17]

        xtree = et.parse("news/" + filename)
        # xroot = xtree.getroot()
        #
        # element = xroot.getchildren()[0]
        # items = element.findall("item")
        xroot = xtree.getroot()[0]
        items = xroot.findall("item")

        for node in items:
            # print("Adding GUID: " + node.find("guid").text)
            ticker.append(_ticker)
            year.append(_year)
            month.append(_month)
            date.append(_date)
            timestamp.append(_timestamp)
            guid.append(node.find("guid").text)
            description.append(node.find("description").text)
            link.append(node.find("link").text)
            pubDate.append(node.find("pubDate").text)
            title.append(node.find("title").text)
    except et.ParseError:
        print("Error in file: " + filename + " (Skipping...)")
        error_counter = error_counter + 1

df_temp = pd.DataFrame(
    {
        'guid': guid,
        'ticker': ticker,
        'year': year,
        'month': month,
        'date': date,
        'timestamp': timestamp,
        'description': description,
        'link': link,
        'pubDate': pubDate,
        'title': title
    }
)
print("Processing Complete! Error in " + str(error_counter) + " files...")
df_temp.head()

df_temp['ticker'].unique()
df_temp.shape
df_temp.to_csv('News_XML_to_DF.csv')
# df_temp.to_csv('/content/drive/MyDrive/Colab Notebooks/datasets/News_XML_to_DF.csv')

##########################
#importing and processing
##########################

df_news = pd.read_csv('News_XML_to_DF.csv')
# df_news = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/datasets/News_Processed_Data/News_XML_to_DF.csv')
df_news.head()

def getSubjectivity(text):
  return TextBlob(text).sentiment.subjectivity

def getPolarity(text):
  return TextBlob(text).sentiment.polarity

df_news['desc_subjectivity'] = df_news['description'].apply(getSubjectivity)
df_news['desc_polarity'] = df_news['description'].apply(getPolarity)
df_news['title_subjectivity'] = df_news['title'].apply(getSubjectivity)
df_news['title_polarity'] = df_news['title'].apply(getPolarity)
df_news.drop(columns=['Unnamed: 0'], axis=0, inplace=True)
df_news.to_csv('News_XML_to_DF_processed.csv')
df_news.head(2)

df_news['date'].unique()
def exportIntoDiffTickers(ticker):
  df_temp_ticker = df_news[df_news['ticker'] == ticker]
  df_temp_ticker.to_csv('News_XML_to_DF_processed_' + ticker + '.csv')

tickers = df_news['ticker'].unique()
for ticker in tickers:
  exportIntoDiffTickers(ticker)

  month_dir = {
      "Jan": "01",
      "Feb": "02",
      "Mar": "03",
      "Apr": "04",
      "May": "05",
      "Jun": "06",
      "Jul": "07",
      "Aug": "08",
      "Sep": "09",
      "Oct": "10",
      "Nov": "11",
      "Dec": "12",
  }


  def format_date(input_date):
      date = input_date[5:16]
      main_date = date[0:2]
      month = date[3:6]
      year = date[7:]
      month = month_dir[month]
      return pd.to_datetime(year + "-" + month + "-" + main_date)


  format_date(df_news['pubDate'][0])
  # Timestamp('2021-04-04 00:00:00')
  df_news['date_formatted'] = df_news['pubDate'].apply(format_date)


  def processForAllStocks(stockTicker):
      df_exported_stockTicker = df_news[df_news['ticker'] == stockTicker]
      df_exported_stockTicker = df_exported_stockTicker.sort_values(by='date_formatted')
      dict = {
          'date': [],
          'ticker_symbol': [],
          'mean_title_subjectivity': [],
          'mean_title_polarity': [],
          'mean_desc_subjectivity': [],
          'mean_desc_polarity': []
      }

      df_processed_stockTicker = pd.DataFrame(dict)
      unique_dates = df_exported_stockTicker.date_formatted.unique()
      for date in unique_dates:
          df_temp_1 = df_exported_stockTicker[df_exported_stockTicker['date_formatted'] == date]
          dfSize = df_temp_1.size

          mean_title_subjectivity = -1
          total_title_subjectivity = 0
          mean_title_polarity = -1
          total_title_polarity = 0

          mean_desc_subjectivity = -1
          total_desc_subjectivity = 0
          mean_desc_polarity = -1
          total_desc_polarity = 0

          for index, row in df_temp_1.iterrows():
              total_subjectivity = total_subjectivity + row['Subjectivity']
              total_polarity = total_polarity + row['Polarity']
              total_engagement = total_engagement + row['comment_num'] + row['retweet_num'] + row['like_num']

          avg_subjectivity = total_subjectivity / dfSize
          avg_polarity = total_polarity / dfSize

          df_temp_2 = {
              'date': date,
              'ticker_symbol': stockTicker,
              'mean_title_subjectivity': mean_title_subjectivity,
              'mean_title_polarity': mean_title_polarity,
              'mean_desc_subjectivity': mean_desc_subjectivity,
              'mean_desc_polarity': mean_desc_polarity
          }
          df_processed_stockTicker = df_processed_stockTicker.append(df_temp_2, ignore_index=True)
          df_processed_stockTicker.to_csv(
                stockTicker + '_daywise.csv')

      # To update URL as follow:
      # df_processed_stockTicker.to_csv(
      #     '/content/drive/MyDrive/Colab Notebooks/datasets/News_Processed_Data/daywise/' + stockTicker + '_daywise.csv')