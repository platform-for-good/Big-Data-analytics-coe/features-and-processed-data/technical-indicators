# Technical Indicators

Heuristic or mathematical calculations based on the historical price, volume, or open interest of a asset. Technical Analysts look for this to judge entry and exit points for trades and predict future price movements.