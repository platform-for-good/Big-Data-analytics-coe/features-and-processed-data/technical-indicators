#spark-submit spark_bollinger_bands.py -t DJIX -s ../../../storage/s1/securities/1d/ -d ../../../storage/s2/bollinger_bands/
import os
import pandas as pd
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, DateType, DoubleType
from argparse import ArgumentParser
from ta.volatility import BollingerBands

parser = ArgumentParser(description='TA')
parser.add_argument('-t', '--tickers', default='DJIX', type=str, required=False, help='Used to look up a specific tickers. Commma seperated. Example: MSFT,AAPL,AMZN')
parser.add_argument('-p', '--period', default=20, type=int, required=False, help='Number of day to compute simple moving average')
parser.add_argument('-sd', '--stdev', default=2, type=float, required=False, help='Number of standard deviation')
parser.add_argument('-s', '--src', default='../../../storage/s1/securities/1d/', type=str, required=False, help='Source directory of the data to be found.')
parser.add_argument('-d', '--dest', default='../../../storage/s2/raw/bollinger_bands/', type=str, required=False, help='Destination directory of the processed to be stored.')

args = parser.parse_args()
print("Tickers " + args.tickers)
print("Source " + args.src)
print("Destination " + args.dest)

if (args.tickers == "DJIX"):
	tickers = ['MMM','AXP','AMGN','AAPL','BA','CAT','CVX','CSCO','KO','DOW','GS','HD','HON','IBM','INTC','JNJ','JPM','MCD','MRK','MSFT','NKE','PG','CRM','TRV','UNH','VZ','V','WBA','WMT','DIS']
elif (args.tickers == "SPX"):
    tickers = ["MMM","ABT","ABBV","ABMD","ACN","ATVI","ADBE","AMD","AAP","AES","AFL","A","APD","AKAM","ALK","ALB","ARE","ALXN","ALGN","ALLE","LNT","ALL","GOOGL","GOOG","MO","AMZN","AMCR","AEE","AAL","AEP","AXP","AIG","AMT","AWK","AMP","ABC","AME","AMGN","APH","ADI","ANSS","ANTM","AON","AOS","APA","AAPL","AMAT","APTV","ADM","ANET","AJG","AIZ","T","ATO","ADSK","ADP","AZO","AVB","AVY","BKR","BLL","BAC","BK","BAX","BDX","BRK.B","BBY","BIO","BIIB","BLK","BA","BKNG","BWA","BXP","BSX","BMY","AVGO","BR","BF.B","CHRW","COG","CDNS","CPB","COF","CAH","KMX","CCL","CARR","CTLT","CAT","CBOE","CBRE","CDW","CE","CNC","CNP","CERN","CF","SCHW","CHTR","CVX","CMG","CB","CHD","CI","CINF","CTAS","CSCO","C","CFG","CTXS","CLX","CME","CMS","KO","CTSH","CL","CMCSA","CMA","CAG","COP","ED","STZ","COO","CPRT","GLW","CTVA","COST","CCI","CSX","CMI","CVS","DHI","DHR","DRI","DVA","DE","DAL","XRAY","DVN","DXCM","FANG","DLR","DFS","DISCA","DISCK","DISH","DG","DLTR","D","DPZ","DOV","DOW","DTE","DUK","DRE","DD","DXC","EMN","ETN","EBAY","ECL","EIX","EW","EA","EMR","ENPH","ETR","EOG","EFX","EQIX","EQR","ESS","EL","ETSY","EVRG","ES","RE","EXC","EXPE","EXPD","EXR","XOM","FFIV","FB","FAST","FRT","FDX","FIS","FITB","FE","FRC","FISV","FLT","FLIR","FLS","FMC","F","FTNT","FTV","FBHS","FOXA","FOX","BEN","FCX","GPS","GRMN","IT","GD","GE","GIS","GM","GPC","GILD","GL","GPN","GS","GWW","HAL","HBI","HIG","HAS","HCA","PEAK","HSIC","HSY","HES","HPE","HLT","HFC","HOLX","HD","HON","HRL","HST","HWM","HPQ","HUM","HBAN","HII","IEX","IDXX","INFO","ITW","ILMN","INCY","IR","INTC","ICE","IBM","IP","IPG","IFF","INTU","ISRG","IVZ","IPGP","IQV","IRM","JKHY","J","JBHT","SJM","JNJ","JCI","JPM","JNPR","KSU","K","KEY","KEYS","KMB","KIM","KMI","KLAC","KHC","KR","LB","LHX","LH","LRCX","LW","LVS","LEG","LDOS","LEN","LLY","LNC","LIN","LYV","LKQ","LMT","L","LOW","LUMN","LYB","MTB","MRO","MPC","MKTX","MAR","MMC","MLM","MAS","MA","MKC","MXIM","MCD","MCK","MDT","MRK","MET","MTD","MGM","MCHP","MU","MSFT","MAA","MHK","TAP","MDLZ","MPWR","MNST","MCO","MS","MOS","MSI","MSCI","NDAQ","NTAP","NFLX","NWL","NEM","NWSA","NWS","NEE","NLSN","NKE","NI","NSC","NTRS","NOC","NLOK","NCLH","NOV","NRG","NUE","NVDA","NVR","ORLY","OXY","ODFL","OMC","OKE","ORCL","OTIS","PCAR","PKG","PH","PAYX","PAYC","PYPL","PNR","PBCT","PEP","PKI","PRGO","PFE","PM","PSX","PNW","PXD","PNC","POOL","PPG","PPL","PFG","PG","PGR","PLD","PRU","PEG","PSA","PHM","PVH","QRVO","PWR","QCOM","DGX","RL","RJF","RTX","O","REG","REGN","RF","RSG","RMD","RHI","ROK","ROL","ROP","ROST","RCL","SPGI","CRM","SBAC","SLB","STX","SEE","SRE","NOW","SHW","SPG","SWKS","SLG","SNA","SO","LUV","SWK","SBUX","STT","STE","SYK","SIVB","SYF","SNPS","SYY","TMUS","TROW","TTWO","TPR","TGT","TEL","TDY","TFX","TER","TSLA","TXN","TXT","TMO","TJX","TSCO","TT","TDG","TRV","TRMB","TFC","TWTR","TYL","TSN","UDR","ULTA","USB","UAA","UA","UNP","UAL","UNH","UPS","URI","UHS","UNM","VLO","VAR","VTR","VRSN","VRSK","VZ","VRTX","VFC","VIAC","VTRS","V","VNT","VNO","VMC","WRB","WAB","WMT","WBA","DIS","WM","WAT","WEC","WFC","WELL","WST","WDC","WU","WRK","WY","WHR","WMB","WLTW","WYNN","XEL","XRX","XLNX","XYL","YUM","ZBRA","ZBH","ZION","ZTS"]
else:
	tickers = args.tickers.split(",")

spark = SparkSession.builder.appName("bollinger_bands").getOrCreate()

DATE = 'Date'
OPEN = 'Open'
HIGH = 'High'
LOW = 'Low'
CLOSE = 'Close'
ADJUSTED_CLOSE = 'Adj Close'
VOLUME = 'Volume'

schema = (StructType([
    StructField(DATE,DateType(),True),               # 0
    StructField(OPEN,DoubleType(),True),             # 1
    StructField(HIGH,DoubleType(),True),             # 2
    StructField(LOW,DoubleType(),True),              # 3
    StructField(CLOSE,DoubleType(),True),            # 4
    StructField(ADJUSTED_CLOSE,DoubleType(),True),   # 5
    StructField(VOLUME,IntegerType(),True)           # 6
    ]))

list_files = [];
for ticker in tickers:
    list_files.append(args.src + ticker + '.csv')
print(list_files)

for stock in list_files:
    print(stock);
    company = ((os.path.basename(stock)).split(".csv")[0])

    df = spark.read.format('csv') \
            .schema(schema) \
            .option("header", "true") \
            .option("sep", ",") \
            .load(stock)

    hist_data = df.toPandas()

    # hist_data = pd.read_csv(path, sep=',')
    # Clean NaN values
    # hist_data = dropna(hist_data)

    indicator_bb = BollingerBands(close=hist_data[CLOSE], window=args.period, window_dev=args.stdev)

    # Add Bollinger Bands features
    hist_data['bb_sma'] = indicator_bb.bollinger_mavg()
    hist_data['bb_high'] = indicator_bb.bollinger_hband()
    hist_data['bb_low'] = indicator_bb.bollinger_lband()

    # Add Bollinger Band high indicator
    hist_data['bb_high_ind'] = indicator_bb.bollinger_hband_indicator()

    # Add Bollinger Band low indicator
    hist_data['bb_low_ind'] = indicator_bb.bollinger_lband_indicator()

    # Add Width Size Bollinger Bands
    hist_data['bb_width'] = indicator_bb.bollinger_wband()

    # Add Percentage Bollinger Bands
    hist_data['bb_percent'] = indicator_bb.bollinger_pband()

    # print(hist_data)
    df = spark.createDataFrame(hist_data)
    # df.show()

    # default
    df.write.format("csv").option("header", "true").option("delimiter", ",").mode('Overwrite').save(
        args.dest + company)
    hist_data.to_csv(args.dest + company + '_summary.csv', index=False)