#spark-submit spark_pivot_point.py -t DJIX -s ../../../storage/s1/securities/1d/ -d ../../../storage/s2/pivot_point/

import os
import pandas as pd
from datetime import datetime
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType, StructField, IntegerType, DateType, DoubleType
from argparse import ArgumentParser
from pyspark.sql import functions as F

parser = ArgumentParser(description='Pivot Point')
parser.add_argument('-t', '--tickers', default='DJIX', type=str, required=False, help='Used to look up a specific tickers. Commma seperated. Example: MSFT,AAPL,AMZN')
parser.add_argument('-s', '--src', default='../../../storage/s1/securities/1d/', type=str, required=False, help='Source directory of the data to be found.')
parser.add_argument('-d', '--dest', default='../../../storage/s2/pivot_point/', type=str, required=False, help='Destination directory of the processed to be stored.')

args = parser.parse_args()
print("Ticker " + args.tickers)
print("Source " + args.src)
print("Destination " + args.dest)

if (args.tickers == "DJIX"):
	tickers = ['MMM','AXP','AMGN','AAPL','BA','CAT','CVX','CSCO','KO','DOW','GS','HD','HON','IBM','INTC','JNJ','JPM','MCD','MRK','MSFT','NKE','PG','CRM','TRV','UNH','VZ','V','WBA','WMT','DIS']
elif (args.tickers == "SPX"):
    tickers = ["MMM","ABT","ABBV","ABMD","ACN","ATVI","ADBE","AMD","AAP","AES","AFL","A","APD","AKAM","ALK","ALB","ARE","ALXN","ALGN","ALLE","LNT","ALL","GOOGL","GOOG","MO","AMZN","AMCR","AEE","AAL","AEP","AXP","AIG","AMT","AWK","AMP","ABC","AME","AMGN","APH","ADI","ANSS","ANTM","AON","AOS","APA","AAPL","AMAT","APTV","ADM","ANET","AJG","AIZ","T","ATO","ADSK","ADP","AZO","AVB","AVY","BKR","BLL","BAC","BK","BAX","BDX","BRK.B","BBY","BIO","BIIB","BLK","BA","BKNG","BWA","BXP","BSX","BMY","AVGO","BR","BF.B","CHRW","COG","CDNS","CPB","COF","CAH","KMX","CCL","CARR","CTLT","CAT","CBOE","CBRE","CDW","CE","CNC","CNP","CERN","CF","SCHW","CHTR","CVX","CMG","CB","CHD","CI","CINF","CTAS","CSCO","C","CFG","CTXS","CLX","CME","CMS","KO","CTSH","CL","CMCSA","CMA","CAG","COP","ED","STZ","COO","CPRT","GLW","CTVA","COST","CCI","CSX","CMI","CVS","DHI","DHR","DRI","DVA","DE","DAL","XRAY","DVN","DXCM","FANG","DLR","DFS","DISCA","DISCK","DISH","DG","DLTR","D","DPZ","DOV","DOW","DTE","DUK","DRE","DD","DXC","EMN","ETN","EBAY","ECL","EIX","EW","EA","EMR","ENPH","ETR","EOG","EFX","EQIX","EQR","ESS","EL","ETSY","EVRG","ES","RE","EXC","EXPE","EXPD","EXR","XOM","FFIV","FB","FAST","FRT","FDX","FIS","FITB","FE","FRC","FISV","FLT","FLIR","FLS","FMC","F","FTNT","FTV","FBHS","FOXA","FOX","BEN","FCX","GPS","GRMN","IT","GD","GE","GIS","GM","GPC","GILD","GL","GPN","GS","GWW","HAL","HBI","HIG","HAS","HCA","PEAK","HSIC","HSY","HES","HPE","HLT","HFC","HOLX","HD","HON","HRL","HST","HWM","HPQ","HUM","HBAN","HII","IEX","IDXX","INFO","ITW","ILMN","INCY","IR","INTC","ICE","IBM","IP","IPG","IFF","INTU","ISRG","IVZ","IPGP","IQV","IRM","JKHY","J","JBHT","SJM","JNJ","JCI","JPM","JNPR","KSU","K","KEY","KEYS","KMB","KIM","KMI","KLAC","KHC","KR","LB","LHX","LH","LRCX","LW","LVS","LEG","LDOS","LEN","LLY","LNC","LIN","LYV","LKQ","LMT","L","LOW","LUMN","LYB","MTB","MRO","MPC","MKTX","MAR","MMC","MLM","MAS","MA","MKC","MXIM","MCD","MCK","MDT","MRK","MET","MTD","MGM","MCHP","MU","MSFT","MAA","MHK","TAP","MDLZ","MPWR","MNST","MCO","MS","MOS","MSI","MSCI","NDAQ","NTAP","NFLX","NWL","NEM","NWSA","NWS","NEE","NLSN","NKE","NI","NSC","NTRS","NOC","NLOK","NCLH","NOV","NRG","NUE","NVDA","NVR","ORLY","OXY","ODFL","OMC","OKE","ORCL","OTIS","PCAR","PKG","PH","PAYX","PAYC","PYPL","PNR","PBCT","PEP","PKI","PRGO","PFE","PM","PSX","PNW","PXD","PNC","POOL","PPG","PPL","PFG","PG","PGR","PLD","PRU","PEG","PSA","PHM","PVH","QRVO","PWR","QCOM","DGX","RL","RJF","RTX","O","REG","REGN","RF","RSG","RMD","RHI","ROK","ROL","ROP","ROST","RCL","SPGI","CRM","SBAC","SLB","STX","SEE","SRE","NOW","SHW","SPG","SWKS","SLG","SNA","SO","LUV","SWK","SBUX","STT","STE","SYK","SIVB","SYF","SNPS","SYY","TMUS","TROW","TTWO","TPR","TGT","TEL","TDY","TFX","TER","TSLA","TXN","TXT","TMO","TJX","TSCO","TT","TDG","TRV","TRMB","TFC","TWTR","TYL","TSN","UDR","ULTA","USB","UAA","UA","UNP","UAL","UNH","UPS","URI","UHS","UNM","VLO","VAR","VTR","VRSN","VRSK","VZ","VRTX","VFC","VIAC","VTRS","V","VNT","VNO","VMC","WRB","WAB","WMT","WBA","DIS","WM","WAT","WEC","WFC","WELL","WST","WDC","WU","WRK","WY","WHR","WMB","WLTW","WYNN","XEL","XRX","XLNX","XYL","YUM","ZBRA","ZBH","ZION","ZTS"]
else:
	tickers = args.tickers.split(",")

spark = SparkSession.builder.appName("pivot_point").getOrCreate()

DATE = 'Date'
OPEN = 'Open'
HIGH = 'High'
LOW = 'Low'
CLOSE = 'Close'
ADJUSTED_CLOSE = 'Adj Close'
VOLUME = 'Volume'

schema = (StructType([
    StructField(DATE,DateType(),True),               # 0
    StructField(OPEN,DoubleType(),True),             # 1
    StructField(HIGH,DoubleType(),True),             # 2
    StructField(LOW,DoubleType(),True),              # 3
    StructField(CLOSE,DoubleType(),True),            # 4
    StructField(ADJUSTED_CLOSE,DoubleType(),True),   # 5
    StructField(VOLUME,IntegerType(),True)           # 6
    ]))

list_files = [];
for ticker in tickers:
    list_files.append(args.src + ticker + '.csv')
print(list_files)

summary = pd.DataFrame(columns=["ticker", "date", "open", "high", "low", "close", "pivot", "s1", "r1", "s2", "r2", "s3", "r3"])
for stock in list_files:
    print(stock);
    company = ((os.path.basename(stock)).split(".csv")[0])

    df = spark.read.format('csv') \
                    .schema(schema) \
                    .option("header","true") \
                    .option("sep",",") \
                    .load(stock)

    expr = [F.last(col).alias(col) for col in df.columns]
    last_row_df = df.agg(*expr)
    last_row = last_row_df.collect()

    PIVOT = 'Pivot'
    date = last_row[0][0]
    open  = last_row[0][1]
    high = last_row[0][2]
    low = last_row[0][3]
    close = last_row[0][4]
    volume = last_row[0][6]

    pivot = (high + low + close) / 3
    r1 = 2 * pivot - low
    s1 = 2 * pivot - high
    r2 = pivot + (high - low)
    s2 = pivot - (high - low)
    r3 = pivot + 2 * (high - low)
    s3 = pivot - 2 * (high - low)

    result = [{
        "date": str(date),
        "open": open,
        "high": high,
        "low": low,
        "close": close,
        "pivot": pivot,
        "s1" : s1,
        "r1" : r1,
        "s2" : s2,
        "r2" : r2,
        "s3" : s3,
        "r3" : r3,
    }]

    summary = summary.append({"ticker" : company, "date" : date.strftime("%d-%b-%y"), "open" : open, "high" : high, "low" : low, "close" : low, "pivot" : pivot, "s1" : s1, "r1" : r1, "s2" : s2, "r2" : r2, "s3" : s3, "r3" : r3}, ignore_index=True)
    # pdf = df.toPandas() #df.sort(df.Date.asc()).toPandas()
    # last_day = pdf.tail(1).copy()
    # last_day[PIVOT] = (last_day[HIGH] + last_day[LOW] + last_day[CLOSE]) / 3
    # last_day['R1'] = 2 * last_day[PIVOT] - last_day[LOW]
    # last_day['S1'] = 2 * last_day[PIVOT] - last_day[HIGH]
    # last_day['R2'] = last_day[PIVOT] + (last_day[HIGH] - last_day[LOW])
    # last_day['S2'] = last_day[PIVOT] - (last_day[HIGH] - last_day[LOW])
    # last_day['R3'] = last_day[PIVOT] + 2 * (last_day[HIGH] - last_day[LOW])
    # last_day['S3'] = last_day[PIVOT] - 2 * (last_day[HIGH] - last_day[LOW])
    #
    # print(last_day)
    #
    # df = spark.createDataFrame(last_day)
    # df.write.format('json').mode('Overwrite').save(args.dest + ticker)

    df = spark.createDataFrame(result)
    # df.show()
    df.write.format('csv').mode('Overwrite').save(args.dest + company)

    summary.to_csv(args.dest + datetime.today().strftime('%Y-%m-%d') + '_summary.csv', index=False)
